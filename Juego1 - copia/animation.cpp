#include "animation.h"

animation::animation(QString nom_img, int _w, int _h,int _time, int num_sprites)
{
    pix.load(":Obs/Objects/"+nom_img+".png");//Pixmad para agregar la imagen animada
    w=_w;
    h=_h;
    numSprites=num_sprites;
    tAnimation = new QTimer(this);//timer de la animacion
    connect(tAnimation,SIGNAL(timeout()),this,SLOT(nextFrame()));//Conexion con el slot nextframe
    tAnimation->start(_time);
    mCurrentFrame=0;//Variable para recorrer la animacion horizontal/
}

QRectF animation::boundingRect() const
{
    return QRectF(0,0,w,h);
}

void animation::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->drawPixmap(0,0,pix,mCurrentFrame,0,w,h);//Dibujar el pixmap con las posiciones en x,y, el recorrido en x (mcurrentFRame), el recorrido en y=0, ancho y alto
    setTransformOriginPoint(boundingRect().topLeft());
}

void animation::nextFrame()
{
    mCurrentFrame +=w;//mcurrentframe obtiene hasta el ancho de la imagen
    if (mCurrentFrame >= w*numSprites) {//Si ya recorrio el doble
        mCurrentFrame = 0;//Se indica que ya termino el recorrido colocando la variable en cero
    }
}
