#ifndef ANIMATION_H
#define ANIMATION_H

#include <QGraphicsItem>
#include <QPainter>
#include <QPixmap>
#include <QObject>
#include <QTimer>

class animation : public QGraphicsObject
{
    Q_OBJECT

public:
    animation(QString nom_img, int _w, int _h, int time, int num_sprites);//Coonstructor que recibe el nombre de la imagen, las dimensiones, el timer para hacer la animacion y el numero sprites
    QRectF boundingRect() const;
    void paint(QPainter *painter,const QStyleOptionGraphicsItem *,QWidget *);

public slots:
    void nextFrame();//Slot que sera llamado con dicho timer
private:
    int mCurrentFrame;
    int w, h, numSprites;
    QPixmap pix;
    QTimer *tAnimation;
};

#endif // ANIMATION_H
