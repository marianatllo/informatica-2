#include "bullets.h"

Bullets::Bullets(double xi, double yi, double xf, double yf, int _diretion, QString nomArch)
{
    diretion=_diretion;
    moveB = new moveBullet(xi,yi,xf,yf,_diretion);
    setPos(xi,-yi);
    pix.load(":/Bullets/Bullets/"+nomArch+".png");
}

Bullets::~Bullets()
{
    delete moveB;
}

QRectF Bullets::boundingRect() const
{
    return QRectF(0,0,pix.width(),pix.height());//Posicion 0,0 y limites ancho y alto
}

void Bullets::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->drawPixmap(0,0,pix.width(),pix.height(),pix);//Posicion 0,0 y limites ancho y alto
}

int Bullets::getDiretion()
{
    return diretion;
}

void Bullets::moveBulletParabolic()
{
    moveB->movementBullet(0.1); //llamo la funcion de la clase movebullet, que recibe el dt
    moveB->setFriccion(friccion);//Le mando el valor de la friccion a set friccion de la clase movebullet
    setRotation(-moveB->getAngleDeg());//funcion setRotacion que me rota el objeto moveB que es de tipo movebullet segun los grados que tengo en getAngleDeg
    setPos(moveB->getX(),-moveB->getY());//Actualizo la posicion de mi obeto moveB

}


double Bullets::getFriccion() const
{
    return friccion;
}

void Bullets::setFriccion(double value)
{
    friccion = value;
}

