#ifndef BULLETS_H
#define BULLETS_H

#include <QGraphicsItem>
#include <QPainter>
#include <QObject>
#include "movebullet.h"
#include "objects.h"


class Bullets : public QGraphicsItem
{
public:
    Bullets(double xi, double yi, double xf, double yf, int _diretion, QString nomArch); //constructor que recibe posiciones iniciales y finales, la direccion de la bala y el nombre de la imagen de la bala a utilizar
    ~Bullets();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
    int getDiretion();//Se obtiene la direccion a donde va a salir la bala
    void moveBulletParabolic();//funcion que me determina el mvto parabolico

    double getFriccion() const;
    void setFriccion(double value);

private:
    QPixmap pix; //creo mi variable pix de pixmap
    moveBullet *moveB; //creo una variable tipo movebullet
    int diretion;
    double friccion;
};

#endif // BULLETS_H
