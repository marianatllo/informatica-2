#include "buttons.h"

Buttons::Buttons(int _x, int _y, int _w, int _h, QString _value)
{

    w=_w;
    h=_h;
    setPos(_x,-_y);//Actualizar posicion
    value=_value;
}

QRectF Buttons::boundingRect() const
{
    return QRectF(0,0,w,h);
}

void Buttons::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen pen;
    //pen.setColor(Qt::red);
    pen.setStyle(Qt::NoPen); //instruccion que no me pinta lo botones en la escena, los crea pero invisibles
    painter->setPen(pen);//agrego el pen a mi painter
    painter->drawRect(boundingRect());
}

void Buttons::mousePressEvent(QGraphicsSceneMouseEvent *)
{
    emit clicked();//Señal que emite el mouse al momento de hacer click
}

void Buttons::setValue(QString _value){
    value=_value;
}

QString Buttons::getValue(void) const {
    return value;
}

