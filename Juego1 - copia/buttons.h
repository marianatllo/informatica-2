#ifndef BUTTONS_H
#define BUTTONS_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QMouseEvent>

class Buttons : public QGraphicsObject
{
    Q_OBJECT

public:
    Buttons(int _x, int _y, int _w, int _h, QString _value="");//constructor que recibe posicion en x,y y ancho y alto
    QRectF boundingRect() const;
    void paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mousePressEvent(QGraphicsSceneMouseEvent *);//Funcion para los eventos que realice con el mouse
    void setValue(QString _value);
    QString getValue() const;
signals:
    void clicked();//Señal que me me envía al momento de dar click en los botones
private:
    int w,h;
    QString value;
};

#endif // BUTTONS_H
