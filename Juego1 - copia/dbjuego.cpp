#include "dbjuego.h"

DBJuego::DBJuego()
{
    //condicionales que me verifican que la conexion, la crwacion de base de datos fue correcta, se procede a crear tabla
    if(conexionBaseDatos()){
        if(crearBaseDatos()){
            crearTabla();
        }
    }
}

bool DBJuego::conexionBaseDatos()
{
    QSqlQuery query;//se crea un nuevo query(llave)
    qDebug()<<"Cargando driver";
    if(QSqlDatabase::isDriverAvailable("QSQLITE")){//Si en la base de datos el driver es disponible es exitosa la conexion a la base de datos
        qDebug()<<"(1) - Exitoso";
    }else {
        qDebug()<<"(1) - Error";
    }

    return true;
}

bool DBJuego::crearBaseDatos()
{
    baseDatos = QSqlDatabase::addDatabase("QSQLITE");//Creo una nueva base datos
    baseDatos.setDatabaseName("worms.sqlite");//Le asigno un nombre

    qDebug()<<"Crear Base de Datos";
    if(baseDatos.open()){//Se abre la base de datos
        qDebug()<<"(2) - Exitoso";
    }else {
        qDebug()<<"(2) - Error";
    }
    return true;
}

bool DBJuego::crearTabla()
{
    QString crear;
    QSqlQuery query;

    //string que se le va a enviar a query
    crear = "CREATE TABLE IF NOT EXISTS jugador("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "nombre VARCHAR(20) UNIQUE)";

    query.prepare(crear);

    qDebug()<<"Crear Tabla";
    if(query.exec()){//creacion de tabla sea exitoso
        qDebug()<<"(3) - Exitoso";
    }else {
        qDebug()<<"(3) - Error"<<query.lastError();
    }
    return true;
}

bool DBJuego::insertar(QString _name)
{
    QString enviar;
    QSqlQuery query;

    //segudno string que se le va a enviar a query
    enviar =    "INSERT INTO jugador (nombre)"
                "VALUES ('"+_name+"')";

    query.prepare(enviar);

    qDebug()<<"Insertar";
    if(query.exec()){
        qDebug()<<"(4) - Exitoso";
        return true;
    }else {
        qDebug()<<"(4) - Error"<<query.lastError();
        return false;
    }
}

QSqlQuery DBJuego::consultar(QString _name)
{
    QString traer;
    QSqlQuery query;
    traer = "SELECT * FROM jugador WHERE nombre = '"+_name+"'";

    query.prepare(traer);

    qDebug()<<"Consultar";
    if(query.exec() && query.next()){
        qDebug()<<"(4) - Exitoso";//<<query.value("nombre").toString();
    }else {
        qDebug()<<"(4) - Error"<<query.lastError();
        //return false;
    }
    return query;
}

