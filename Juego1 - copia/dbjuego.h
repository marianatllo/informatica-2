#ifndef DBJUEGO_H
#define DBJUEGO_H

#include <QtSql>

class DBJuego
{
public:
    DBJuego();//constructor
    enum Campos{ //Declaracion de un tipo de enumeracion
        ID,
        NOMBRE,
        NUM_CAMPOS // Siempre debe estar al final
    };
private:
    QSqlDatabase baseDatos; //Clase base de datos
    bool conexionBaseDatos(void);
    bool crearBaseDatos(void);
    bool crearTabla();

public slots:
    bool insertar(QString _name);
    QSqlQuery consultar(QString _name);
};

#endif // DBJUEGO_H
