#include "widget.h"
#include <QApplication>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
    srand(time(NULL));

    QApplication a(argc, argv);
    Widget w;
    w.show();
    w.menuStart();//instruccion para que al ejecutar juego1, se me abra el menu
    return a.exec();
}
