#include "move.h"

Move::Move(double px, double py)
{
    x=px;
    y=py;
    vy=40;//Valor arbitrario para la velocidad en y
}

void Move::movement(double dt)//Mvto en Y del gusano
{
    y+=vy*dt+GRAV*dt*dt/2;
    vy+=GRAV*dt;
}

double Move::getX()
{
    return x;
}

double Move::getY()
{
    return y;
}

double Move::getVy()
{
    return vy;

}

void Move::setX(double _x)
{
    x=_x;
}

void Move::setY(double _y)
{
    y=_y;
}

void Move::setVy(double _vy)
{
    vy=_vy;
}
