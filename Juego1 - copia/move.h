#ifndef MOVE_H
#define MOVE_H

#define GRAV -9.8

class Move
{
public:
    Move(double px, double py);//constructor que me recibe posicion en x,y

    void movement(double dt);//funcion para determinar mvto en Y al momento de que el gusano salte y recibe el dt
    double getX();
    double getY();
    double getVy();

    void setX(double _x);
    void setY(double _y);
    void setVy(double _vy);



private:
    double x,y,vy;
};



#endif // MOVE_H
