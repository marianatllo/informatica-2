#include "movebullet.h"
#include "math.h"
moveBullet::moveBullet(double _xi, double _yi, double _xf, double _yf, int diretion)
{
    x=_xi;
    y=_yi;
    xf=_xf;
    yf=_yf;
    v=sqrt(pow((x-xf),2)+pow((y-yf),2));//Ecuacion para sacar la magnitud del vector v
    limitV();//limite creado para que la bala no salga con mucha velocidad
    double angle = atan((y-yf)/(diretion*(x-xf)));// angulo entre x y el vector v, en donde se tiene en cuenta la direccion si es negativa o positiva
    vx=diretion*v*cos(angle)/2;//velocidad en x
    vy=v*sin(angle)/2;//velocidad en y, se divide entre 2 para evitar que las velocidades sean muy grandes y las balas se salgan de la escena
}

void moveBullet::movementBullet(double dt)
{
    y+=vy*dt+GRAV*dt*dt/2;//Ecuacion para la posicion en y despues de hallar la velocidad en y
    vy+=GRAV*dt;
    x+= vx*dt-friccion;//Ecuacion para la posicion en x, dado que la velocidad en x permanece constante menos la friccion que es la proporcinada por el aire
}

double moveBullet::getV()
{
    return v;
}

double moveBullet::getX()
{
    return x;
}

double moveBullet::getY()
{
    return y;
}

double moveBullet::getAngleDeg()//Esta funcion se utiliza para pasar el angulo hallado en constructor a grados, dado que la funcion setRotacion utiliza el angulo en grados
{
    angDeg = 180*atan(vy/vx)/PI;
    return angDeg;
}

void moveBullet::limitV()//Limite por si la velocidad v de la bala sobrepasa de 200, se convierta automaticamente en 180 y la bala no salga tan disparada de la escena y se pierda
{
    if(v>200){
        v=180;
    }
}

double moveBullet::getFriccion() const
{
    return friccion;
}

void moveBullet::setFriccion(double value)
{
    friccion = value;
}

