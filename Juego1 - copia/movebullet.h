#ifndef MOVEBULLET_H
#define MOVEBULLET_H

#define GRAV -9.8
#define PI 3.1416
#define R 0.9

class moveBullet
{
public:
    moveBullet(double _xi, double _yi, double _xf, double _yf, int diretion);//constructor que me recibe la posicion inicial y final de x,y y la direccion en la que se va a mover la bala
    void movementBullet(double dt);//funcion para el mvto parabolico
    double getV();
    double getX();
    double getY();
    double getAngleDeg();//Metodo get para el angulo que se utiliza en la funcion de rotacion

    double getFriccion() const;
    void setFriccion(double value);

private:
    void limitV();
    double xf, yf, x, y;
    double vx,vy,v,angDeg,friccion;


};

#endif // MOVEBULLET_H
