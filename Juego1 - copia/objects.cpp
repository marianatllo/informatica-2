#include "objects.h"

Objects::Objects(QString nameObject, int px, int py)
{
    setPos(px,-py);//Se actualiza posicion
    nObs = nameObject;
    pix.load(":/Obs/Objects/"+nameObject+".png");//Se agrega la imagen del objeto al pixmap

}

QRectF Objects::boundingRect() const
{
    return QRectF(0,0,pix.width(),pix.height());
}

void Objects::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->drawPixmap(0,0,pix.width(),pix.height(),pix);
}

QString Objects::getNameObject()
{
    return nObs;
}
