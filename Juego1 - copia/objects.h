#ifndef OBJECTS_H
#define OBJECTS_H

#include <QGraphicsItem>
#include <QPainter>

class Objects : public QGraphicsItem
{
public:
    Objects(QString _nameObject, int px, int py);//constructor que recibe el nombre del objeto, y la posicion en x,y donde se va a colocar el objeto
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
    QString getNameObject();
private:
    QPixmap pix;
    QString nObs;

};

#endif // OBJECTS_H
