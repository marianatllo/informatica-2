#ifndef TRIVIA_H
#define TRIVIA_H

#include <QWidget>

namespace Ui {
class trivia;
}

class trivia : public QWidget
{
    Q_OBJECT

public:
    explicit trivia(QWidget *parent = nullptr);
    ~trivia();

private:
    Ui::trivia *ui;
};

#endif // TRIVIA_H
