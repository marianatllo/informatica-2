#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setFixedSize(1003,503);
    layout()->setMargin(0);//instruccion para que no haya margen de la escena

    scene = new QGraphicsScene(this);//creo una nueva escena
    scene->setSceneRect(0,-500,1000,500);//le doy las dimensiones a mi escena
    ui->graphicsView->setScene(scene);//agrego la escena a mi graphicsView


    tShot = new QTimer(this);//Timer para mover las balas
    connect(tShot,SIGNAL(timeout()),this,SLOT(starMoveBullet()));//conexion para que mientras se me ejecute el timer, se ejecute el slot para mover la bala
    tShot->stop();//El timer inicia en stop


    txtRegistro = new QLineEdit(this);//creo un nuevo QlineEdit para hacer uso de ellos en la base de datos
    txtIngreso = new QLineEdit(this);
    dbjuego = new DBJuego();//Creo una nueva base de datos

    //Para agregarle musica al juego
     player = new QMediaPlayer;
     player->setMedia(QUrl::fromLocalFile("Faded.mp3"));
     player->setVolume(30);
     player->play();



}

Widget::~Widget()
{
    delete ui;
    delete obs;
    qDeleteAll(worm_player_one);
    worm_player_one.clear();
    qDeleteAll(worm_player_two);
    worm_player_two.clear();
}

void Widget::menuStart()
{

    QGraphicsPixmapItem *bg1= new QGraphicsPixmapItem(QPixmap("://Backgrounds/bg1.png"));//Agrego el fondo al pixmad
    bg1->setPos(0,-500);//Le asigno una posicion
    scene->addItem(bg1);

    QFont font("Cooper Black",18,QFont::Normal,false);//fuente del texto
    menustart = new QGraphicsTextItem("Sign In");
    menustart->setFont(font);//agrego la fuente
    menustart->setPos(445,-490);//la posicion
    scene->addItem(menustart);//agrego el item a la escena

    //Texto que aparece en la barra para ingresar el nuevo usuario
    txtRegistro->setPlaceholderText("Nuevo usuario");
    txtRegistro->setGeometry(400,50,240,25);
    txtRegistro->show();
    txtIngreso->hide();

    buttonSingIn = new Buttons(445,500,115,45);//Creo un botón con las dimensiones de la posicion en x,y y ancho, alto
    connect(buttonSingIn,SIGNAL(clicked()),this,SLOT(SingIn()));//Conecto el boton con la señal clicked, para que cree el primer nivel
    scene->addItem(buttonSingIn);//Agrego el botón a la escena

    buttonPlay = new Buttons(445,400,115,45);//Creo un botón con las dimensiones de la posicion en x,y y ancho, alto
    connect(buttonPlay,SIGNAL(clicked()),this,SLOT(User()));//Conecto el boton con la señal clicked, para que cree el primer nivel
    scene->addItem(buttonPlay);//Agrego el botón a la escena

    buttonQuit = new Buttons(445,325,115,45);//Creo un segundo botón con las dimensiones de posicion en x,y y ancho,alto
    connect(buttonQuit,SIGNAL(clicked()),this,SLOT(close())); //Conecto el boton con la señal clicked, para que cierre la pestaña actual, la funcion close es propia de QT
    scene->addItem(buttonQuit);//Agrego el botón a la escena

}

void Widget::mousePressEvent(QMouseEvent *m)
{


    if(banBullet== true){ //Si la bala esta en escena
        if(p==WormsPlayerOne::ONE && worm_player_one.at(index_worm_focused)->isEnabled()){//Si el jugador es gusano1 y esta habilitado
            //Se obtiene las posiciones del gusano enfocado
            double xi = worm_player_one.at(index_worm_focused)->x();
            double yi = -worm_player_one.at(index_worm_focused)->y();
            //Se enfoca el gusano
            worm_player_one.at(index_worm_focused)->setFlags(QGraphicsItem::ItemIsFocusable);
            worm_player_one.at(index_worm_focused)->setFocus();
            bullet=new Bullets(xi,yi,m->x(),500-m->y(),1,bulletType);//Se crea una nueva bala donde las posiciones iniiciales son las del gusano y las finales las del evento m del mouse, ademas se indica la direccion de la bala y el nombre
            scene->addItem(bullet);//Se agrega la bala a la escena

            friccion =rand()%5;//numero aleatorios
            qDebug()<<friccion;
            bullet->setFriccion(friccion);
            tShot->start(15);

        }else if(p==WormsPlayerOne::TWO && worm_player_two.at(index_worm_focused)->isEnabled()){//Si el jugador es gusano 2
            //Se obtiene las posiciones del gusano enfocado
            double xi = worm_player_two.at(index_worm_focused)->x();
            double yi = -worm_player_two.at(index_worm_focused)->y();
            //Se enfoca el gusano
            worm_player_two.at(index_worm_focused)->setFlags(QGraphicsItem::ItemIsFocusable);
            worm_player_two.at(index_worm_focused)->setFocus();

            bullet=new Bullets(xi,yi,m->x(),500-m->y(),-1,bulletType+"R");//Se crea una nueva bala donde las posiciones iniiciales son las del gusano y las finales las del evento m del mouse, ademas se indica la direccion de la bala y el nombre
            scene->addItem(bullet);//Se agrega la bala a la escena

            friccion =rand()%3;//numero aleatorios para determinar la friccion del aire
            qDebug()<<friccion;

            scene->removeItem(viento);
            bullet->setFriccion(friccion);//envio la friccion a la clase bullet para afectar el mvto de la misma
            tShot->start(15);
        }
        //Nivel de viento
        QFont font("Cooper Black",14,QFont::Normal,false);//fuente del texto
        if (friccion==1){
            viento = new QGraphicsTextItem("1");
            viento->setFont(font);
            viento->setPos(150,-500);
            scene->addItem(viento);
        }
        else if(friccion==2){
            viento = new QGraphicsTextItem("2");
            viento->setFont(font);
            viento->setPos(150,-500);
            scene->addItem(viento);
        }
        else if(friccion==3){
            viento = new QGraphicsTextItem("3");
            viento->setFont(font);
            viento->setPos(150,-500);
            scene->addItem(viento);
        }
        else if(friccion==4){
            viento = new QGraphicsTextItem("4");
            viento->setFont(font);
            viento->setPos(150,-500);
            scene->addItem(viento);
        }
        else if(friccion==0){
            viento = new QGraphicsTextItem("0");
            viento->setFont(font);
            viento->setPos(150,-500);
            scene->addItem(viento);
        }

        banBullet=false;
    }

}


void Widget::addWormsOne()//Funcion para agregar gusanos a la escena
{
    /*En donde se agrega un nuevo elemento a la lista de gusanos 1 de tipo WormsPlayeOne, donde el constructor
    me recibe las posiciones en x,y en donde quiero ubicar el gusano, la lista de objetos y que tipo de player es*/
    worm_player_one.append(new WormsPlayerOne(300,362,listObs,WormsPlayerOne::ONE));
    worm_player_one.append(new WormsPlayerOne(52,214,listObs,WormsPlayerOne::ONE));
    worm_player_one.append(new WormsPlayerOne(130,443,listObs,WormsPlayerOne::ONE));

    for(int i=0;i<worm_player_one.size(); i++){//ciclo para agregarle a cada gusano creado el objeto installeventfilter y agregar cada gusano a la escena
        worm_player_one.at(i)->installEventFilter(this);//Se instala un filtro de eventos que recibe todos los eventos que se envían al objeto worms
        scene->addItem(worm_player_one.at(i));//Se agrega cada gusano a la escena


    }

}

void Widget::addWormsTwo()
{
    //El mismo procedimiento que se le realiza con los gusanos 1, solo se cambia la posicion en la que salen en la escena y dando a entender que son player2.
    worm_player_two.append(new WormsPlayerOne(850,414,listObs,WormsPlayerOne::TWO));
    worm_player_two.append(new WormsPlayerOne(645,164,listObs,WormsPlayerOne::TWO));
    worm_player_two.append(new WormsPlayerOne(580,399,listObs,WormsPlayerOne::TWO));
    for(int i=0;i< worm_player_two.size(); i++){
        worm_player_two.at(i)->installEventFilter(this);
        scene->addItem( worm_player_two.at(i));
    }
}

bool Widget::eventFilter(QObject *object, QEvent *event){

    if(event->type()==QEvent::FocusIn){//Se condiciona el tipo de evento que esta pasando, en este caso el de enfocar a los gusanos

        for(int i=0;i<worm_player_one.size();i++){//Recorre el tamaño de la lista de los gusanos1
            if(object==worm_player_one.at(i)){//Si el objeto es el gusano en i
                worm_player_one.at(i)->getTextItem()->setDefaultTextColor(Qt::blue);//El texto que se coloca encima de cada gusano sera de color rojo, para identificar el gusano enfocado
                p = worm_player_one.at(i)->getPlayer();//p sera igual al numero de player
                index_worm_focused = i;
                titulo->setPlainText("Player 1");

            }
        }
        for(int i=0;i<worm_player_two.size();i++){//El mismo procedimiento realizado arriba solo que para los gusanos 2
            if(object==worm_player_two.at(i)){
                worm_player_two.at(i)->getTextItem()->setDefaultTextColor(Qt::red);
                p = worm_player_two.at(i)->getPlayer();
                index_worm_focused = i;
                titulo->setPlainText("Player 2");
            }
        }

    }
    if(event->type()==QEvent::FocusOut){//Si el evento es no estar enfocado
        for(int i=0;i<worm_player_one.size();i++){
            if(object==worm_player_one.at(i)){
                worm_player_one.at(i)->getTextItem()->setDefaultTextColor(Qt::darkBlue);//El texto se coloca en solor negro para identificarlos
                p = worm_player_one.at(i)->getPlayer();
                index_worm_focused = i;
            }
        }
        for(int i=0;i<worm_player_two.size();i++){//Se reliza el mismo procedimientos con los gusanos 2
            if(object==worm_player_two.at(i)){
                worm_player_two.at(i)->getTextItem()->setDefaultTextColor(Qt::darkRed);
                p = worm_player_two.at(i)->getPlayer();
                index_worm_focused = i;
            }
        }

    }
    return false;
}

void Widget::collisionBullet()
{
    if(bullet->getDiretion()>0){//Si la direccion de la bala es 1, quiere decir que los gusano que lanzan las balas son los gusanos 1.
        for(int i=0;i<worm_player_two.size();i++){//Por eso se recorre la lista de los gusanos 2.
            if(bullet->collidesWithItem(worm_player_two.at(i))){//Si la bala colisiona con algun gusano 2
                scene->removeItem(bullet);//Se elimina la bala de la escena
                if(bulletType=="banana"){
                    worm_player_two.at(i)->setLife(worm_player_two.at(i)->getLife()-10);//Se le resta 10 a la vida
                    deleteWormsScene(worm_player_two.at(i)->getLife(),i);//Llamo la funcion borrar gusanos, solo para el caso en que la vida sea menor que 1
                    banBullet=true;
                }
                else if (bulletType=="arrow"){
                    worm_player_two.at(i)->setLife(worm_player_two.at(i)->getLife()-20);//Se le resta 20 a la vida
                    deleteWormsScene(worm_player_two.at(i)->getLife(),i);//Llamo la funcion borrar gusanos, solo para el caso en que la vida sea menor que 1
                    banBullet=true;
                }
                else if (bulletType=="airmisl"){
                    worm_player_two.at(i)->setLife(worm_player_two.at(i)->getLife()-5);//Se le resta 5 a la vida
                    deleteWormsScene(worm_player_two.at(i)->getLife(),i);//Llamo la funcion borrar gusanos, solo para el caso en que la vida sea menor que 1
                    banBullet=true;
                }
                else if (bulletType=="bullet"){
                    worm_player_two.at(i)->setLife(worm_player_two.at(i)->getLife()-50);//Se le resta 50 a la vida
                    deleteWormsScene(worm_player_two.at(i)->getLife(),i);//Llamo la funcion borrar gusanos, solo para el caso en que la vida sea menor que 1
                    banBullet=true;
                }

                tShot->stop();//Se pausa el timer de mover las balas

            }
        }
    }
    else if (bullet->getDiretion()<0){//Si la direccion de la bala es -1, quiere decir que los gusano que lanzan las balas son los gusanos 2.
        for(int i=0;i<worm_player_one.size();i++){//Por eso se recorre la lista de los gusanos 1.
            if(bullet->collidesWithItem(worm_player_one.at(i))){//Si la bala colisiona con algun gusano 1
                scene->removeItem(bullet);//Se elimina la bala de la escena
                if(bulletType=="banana"){
                    worm_player_one.at(i)->setLife(worm_player_one.at(i)->getLife()-10);//Se le resta 10 a la vida
                    deleteWormsScene(worm_player_one.at(i)->getLife(),i);//Llamo la funcion borrar gusanos, solo para el caso en que la vida sea menor que 1
                    banBullet=true;
                }
                else if (bulletType=="arrow"){
                    worm_player_one.at(i)->setLife(worm_player_one.at(i)->getLife()-20);//Se le resta 20 a la vida
                    deleteWormsScene(worm_player_one.at(i)->getLife(),i);//Llamo la funcion borrar gusanos, solo para el caso en que la vida sea menor que 1
                    banBullet=true;
                }
                else if (bulletType=="airmisl"){
                    worm_player_one.at(i)->setLife(worm_player_one.at(i)->getLife()-5);//Se le resta 5 a la vida
                    deleteWormsScene(worm_player_one.at(i)->getLife(),i);//Llamo la funcion borrar gusanos, solo para el caso en que la vida sea menor que 1
                    banBullet=true;
                }
                else if (bulletType=="bullet"){
                    worm_player_one.at(i)->setLife(worm_player_one.at(i)->getLife()-50);//Se le resta 50 a la vida
                    deleteWormsScene(worm_player_one.at(i)->getLife(),i);//Llamo la funcion borrar gusanos, solo para el caso en que la vida sea menor que 1
                    banBullet=true;
                }
                tShot->stop();//Se pausa el timer de mover las balas
            }
        }
    }
}

void Widget::deleteWormsScene(int life, int i)//Me recibe la vida del gusano y el numero del gusano
{

    if(life<=0){
        if (p==WormsPlayerOne::ONE){
           scene->removeItem(worm_player_two.at(i));//Se elimina el gusano de la escena
           worm_player_two.removeAt(i);//elimo dicho gusano de la lista
           deadPlayersTwo+=1;//Aumento el contador
           qDebug()<<deadPlayersTwo;
           if(deadPlayersTwo==3){//Si este contador llega a 3, ya existe un ganador
               deadPlayersTwo=0;
               EndGame(1);//Se llama la funcion endGame mandandole el numero del player que gano


           }

        }
        else if(p==WormsPlayerOne::TWO){
            scene->removeItem(worm_player_one.at(i));//Se elimina el gusano de la escena
            worm_player_one.removeAt(i);
            deadPlayersOne+=1;
            qDebug()<<deadPlayersOne;
            if(deadPlayersOne==3){//Si este contador llega a 3, ya existe un ganador
                deadPlayersOne=0;
                EndGame(2);//Se llama la funcion endGame mandandole el numero del player que gano

            }
        }

    }
}

void Widget::shotIsEnable()
{
    if(p==WormsPlayerOne::ONE){//Si es el jugador 1 el que esta enfocado
        for(int i = 0; i < worm_player_one.size(); i++){
            worm_player_one.at(i)->setEnabled(false);//si setenabled es falso inhabilito el item, toda la lista de los gusanos 1
            banBullet=false;
            titulo->setPlainText("Player 2");//Text que aparece arriba de la escena indiciando que jugador esta en turno
            titulo->setOpacity(0.4);//Instruccion para que el texto se vea mas claro
        }
        banBullet=true;
        for(int i = 0; i < worm_player_two.size(); i++){
            worm_player_two.at(i)->setEnabled(true);//Y habilito a los gusanos 2 para que jueguen su turno
            titulo->setPlainText("Player 2");//Text que aparece arriba de la escena indiciando que jugador esta en turno
            titulo->setOpacity(0.4);//Instruccion para que el texto se vea mas claro

        }

    }else if (p==WormsPlayerOne::TWO){//Si es el jugador 2 el que esta enfocado
        for(int i = 0; i < worm_player_two.size(); i++){
            worm_player_two.at(i)->setEnabled(false);//inhabilito el item, toda la lista de los gusanos 2
            banBullet=false;
            titulo->setPlainText("Player 1");//Text que aparece arriba de la escena indiciando que jugador esta en turno
            titulo->setOpacity(0.4);//Instruccion para que el texto se vea mas claro
        }
        banBullet=true;
        for(int i = 0; i < worm_player_one.size(); i++){
            worm_player_one.at(i)->setEnabled(true);//Y habilito a los gusanos 1 para que jueguen su turno
            titulo->setPlainText("Player 1");//Text que aparece arriba de la escena indiciando que jugador esta en turno
            titulo->setOpacity(0.4);//Instruccion para que el texto se vea mas claro
        }

    }
    banBullet=false;
}



void Widget::EndGame(int playerWinner)
{
    QGraphicsPixmapItem *bgWinner= new QGraphicsPixmapItem(QPixmap("://Backgrounds/bgWinner.png")); //Se crea uno nuevo graphics item para agregar un nuevo fondo
    bgWinner->setPos(0,-500);//Se posiciona
    scene->addItem(bgWinner);//Agrego el item

    //Creo un text item para agrega texto al fondo
    QFont font("Cooper Black",40,QFont::Normal,false);
    QGraphicsTextItem *textWinner = new QGraphicsTextItem("Winner Player "+QString::number(playerWinner));//En el texto sale el jugador que ganó
    textWinner->setFont(font);//Se agrega la fuente
    textWinner->setPos(280,-440);//Se agrega la posicion


    //Aparecen tres botones en la escena
    bPlayAgain = new Buttons(376,314,250,45);
    connect(bPlayAgain,SIGNAL(clicked()),this,SLOT(User()));//Si se selecciona el boton de bplayagain, se ejecuta el slot playagain
    scene->addItem(bPlayAgain);//Se agrega el item del boton

    bQuit = new Buttons(448,174,108,45);
    connect(bQuit,SIGNAL(clicked()),this,SLOT(close()));//Si se selecciona el boton de bquit, se ejecuta el slot quit
    scene->addItem(bQuit);//Se agrega el item del boton


    if(playerWinner==1){//Si el ganador es el jugador 1 el texto se pone en azul
        textWinner->setDefaultTextColor(Qt::blue);
    }else {//Si el ganador es el jugador 2 el texto se pone en rojo
        textWinner->setDefaultTextColor(Qt::red);
    }
    scene->addItem(textWinner);//Se agrega el item

}

void Widget::SingIn()
{
    QMessageBox msgBox;//se crea una caja de mensajes
    if(dbjuego->insertar(txtRegistro->text())){//Se llama la base de datos y se le llama al string insertar el texto ingresado
        msgBox.setText("Usuario registrado");
        msgBox.exec();
    }
    else{
        msgBox.setText("El usuario ya existe, digite uno nuevo");
        msgBox.exec();
    }
    txtRegistro->clear();
}

void Widget::User()
{
    //scene->clear();//Limpio la escena
    txtRegistro->hide();

    QGraphicsPixmapItem *bg2= new QGraphicsPixmapItem(QPixmap("://Backgrounds/bg2.png"));//Agrego un nuevo pixmap
    bg2->setPos(0,-500);//le asgino la posicion
    scene->addItem(bg2);//agrego el item

    QFont font("Cooper Black",18,QFont::Normal,false);//fuente del texto
    titulo = new QGraphicsTextItem("User");//El texto dira User
    titulo->setFont(font);//agrego la fuente
    titulo->setPos(445,-490);//la posicion
    scene->addItem(titulo);//agrego el item a la escena


    text = new QGraphicsTextItem("Play");//El texto dira play
    text->setFont(font);//agrego la fuente
    text->setPos(445,-400);//la posicion
    scene->addItem(text);//agrego el item a la escena

    txtIngreso->setPlaceholderText("Ingrese usuario");
    txtIngreso->setGeometry(400,50,240,25);
    txtIngreso->show();

    buttonUser = new Buttons(445,400,100,45);//Creo un botón con las dimensiones de la posicion en x,y y ancho, alto
    connect(buttonUser,SIGNAL(clicked()),this,SLOT(Login()));//Conecto el boton con la señal clicked, para que cree el primer nivel
    scene->addItem(buttonUser);//Agrego el botón a la escena




}

void Widget::Login()
{
    QMessageBox msgBox;//creo de nuevo la caja de mensajes
    QSqlQuery query;
    query = dbjuego->consultar(txtIngreso->text());// a la clave de la base de datos le envio el usuario que debe estar ya registrado

    if(query.value("nombre").toString() == txtIngreso->text() && txtIngreso->text() != ""){//hago una comparacion y si ambos son los mismo dispongo a crear nuevo nivel
        createLevels();
    }
    else{
        msgBox.setText("El usuario no encontrado, registrese.");
        msgBox.exec();
    }
    txtIngreso->clear();


}

void Widget::createLevels()//Funcion que se ejecuta con la señal clicked
{
    //scene->clear();//Limpio la escena
    txtRegistro->hide();
    txtIngreso->hide();
    if(remover){//Remuevo los botones creados en los modulos anteriores
        scene->removeItem(buttonSingIn);
        scene->removeItem(buttonPlay);
        scene->removeItem(buttonQuit);
        scene->removeItem(buttonUser);
        delete buttonSingIn;
        delete buttonPlay;
        delete buttonQuit;
        delete buttonUser;

        remover=false;

    }
    QGraphicsPixmapItem *bgLevels= new QGraphicsPixmapItem(QPixmap("://Backgrounds/bgLevel1.png"));//Agrego un nuevo pixmap
    bgLevels->setPos(0,-500);//le asgino la posicion
    scene->addItem(bgLevels);//agrego el item


    //creo los botones para seleccionar las balas
    buttonBanana = new Buttons(480,499,50,50,"banana");
    buttonArrow = new Buttons(420,499,50,50,"arrow");
    buttonAirmisl = new Buttons(350,499,50,50,"airmisl");
    buttonBullet = new Buttons(550,499,50,50,"bullet");

    scene->addItem(buttonBanana);
    scene->addItem(buttonArrow);
    scene->addItem(buttonAirmisl);
    scene->addItem(buttonBullet);

    //los conecto con el slot selectbullet
    connect(buttonBanana,SIGNAL(clicked()),this,SLOT(selectBullet()));
    connect(buttonArrow,SIGNAL(clicked()),this,SLOT(selectBullet()));
    connect(buttonAirmisl,SIGNAL(clicked()),this,SLOT(selectBullet()));
    connect(buttonBullet,SIGNAL(clicked()),this,SLOT(selectBullet()));

    QFont font("Cooper Black",14,QFont::Normal,false);//fuente del texto
    titulo = new QGraphicsTextItem("Select Turn");//El texto dira seleccione turno
    titulo->setFont(font);//agrego la fuente
    titulo->setPos(700,-490);//la posicion
    scene->addItem(titulo);//agrego el item a la escens

    text = new QGraphicsTextItem("Nivel viento:");
    text->setFont(font);
    text->setPos(2,-500);
    scene->addItem(text);

    //Abrir un archivo que lee los objetos que estaran puestos en escena
    QFile arch;
    arch.setFileName("Level1.txt");//Nombre del archivo
    arch.open(QIODevice::ReadOnly);//Abro el archivo
    QString line;//Creo un string llamado line
    QStringList list;//Creo una lista

    while(true){
        line=arch.readLine();//En line se guardara cada linea  del archivo
        if(!line.isEmpty()){//Si la linea no es vacia

            list=line.split(' ',QString::SkipEmptyParts);//Instruccion para que en la lista quede en cada posicion un string de la linea del archivo
            list[2].remove("\r\n");//Remover el espacio en la posicion 2

        }else{
            break;
        }
        obs = new Objects(list.at(0),list.at(1).toInt(),500-list.at(2).toInt());//Instancio la clase objects llamando al constructor que me recibe el nombre del objeto y las posiciones en x,y, conviertiendo el string de la posicion en entero
        listObs.append(obs);//Agregar los objetos a la lista
        scene->addItem(obs);//Agregarlos a la escena

        list.clear();//Limpiar la lista
    }
    arch.close();//Cerrar el archivo
    //Añadir los gusanos a la escena
    addWormsOne();
    addWormsTwo();
}


void Widget::starMoveBullet()
{
    bullet->moveBulletParabolic();//Llamamos la funcion del mvto parabolico de la bala
    collisionBullet();//Llamo la funcion para mirar si las balas colisionan con los gusanos
    shotIsEnable();//llamo la funcion para seleccionar turnos despues de que la bala sea lanzada
    //Condicionales para mirar si la bala se salio de la escena y asi poder borrarla
    if(bullet->x() > 1000){
        banBullet=true;
        delete bullet;//Se borra la bala
        tShot->stop();
    }else if(bullet->y()>0){
        banBullet=true;
        delete bullet;
        tShot->stop();
    }else if (bullet->x()<0){
        banBullet=true;
        delete bullet;
        tShot->stop();
    }
    banBullet=false;
}

void Widget::selectBullet()
{
    //instrucciones para llevarle a bulletType el nombre de la bala seleccionada
    QObject* sender = this->sender();
    Buttons* button = qobject_cast<Buttons*>(sender);
    bulletType = button->getValue();
    banBullet=true;
}







