#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <QFile>
#include <QTimer>
#include <QKeyEvent>
#include <QLineEdit>
#include <QMessageBox>
#include <QSqlQuery>
#include <QMediaPlayer>

#include "backgrounds.h"
#include "buttons.h"
#include "objects.h"
#include "wormsplayerone.h"
#include "bullets.h"
#include "animation.h"
#include "dbjuego.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT



public:
    explicit Widget(QWidget *parent = nullptr);//Constructor donde creo la escena
    ~Widget();
    void menuStart();//Funcion para el Menu que se ejecuta al inicio del juego
    void mousePressEvent(QMouseEvent *m);//Funcion del mouse para apuntar la bala
    void createLevels();//funcion que me crea el nivel
    void addWormsOne();//Funciones para crear los gusanos de cada banda
    void addWormsTwo();
    bool eventFilter(QObject *object, QEvent *event);//Funcion para instalar el filtro de eventos
    void collisionBullet();//Funcion de colision con las balas
    void deleteWormsScene(int lifem, int i);//Borrar los gusano de la escena depende de la vida
    void shotIsEnable();//funcion para determinar los turnos de los gusanos
    void EndGame(int playerWinner);//funcion para el fin del juego

public slots:
    void SingIn();
    void User();
    void Login();
    void starMoveBullet();
    void selectBullet();



private:
    Ui::Widget *ui;
    QGraphicsScene *scene;
    QPixmap pix;//Creo un pixmap
    Objects *obs;
    QTimer *tShot;//timer para las balas
    QList<Objects *> listObs;//Creo una lista donde van a estar todos los objetos
    //Crear la lista de los gusanos
    QList<WormsPlayerOne *> worm_player_one;
    QList<WormsPlayerOne *> worm_player_two;
    //Crear la bala tipo bullets
    Bullets *bullet;
    QGraphicsTextItem *titulo, *text, *menustart, *viento;
    WormsPlayerOne::Player p = WormsPlayerOne::NOT;//Crear la variable p, de tipo player que es la que va a saber que gusano esta jugando, si 1 o 2
    int index_worm_focused=0;
    bool banBullet = true;
    int deadPlayersOne = 0;//Contadores
    int deadPlayersTwo=0;
    int friccion=0;

    QLineEdit *txtRegistro, *txtIngreso;
    DBJuego *dbjuego;

    QString bulletType;
    bool remover= true;

    Buttons *buttonSingIn;
    Buttons *buttonPlay;
    Buttons *buttonQuit;

    Buttons *buttonUser;

    Buttons *buttonBanana;
    Buttons *buttonArrow;
    Buttons *buttonAirmisl;
    Buttons *buttonBullet;

    Buttons *bPlayAgain;
    Buttons *bQuit;

    QMediaPlayer* player;

};

#endif // WIDGET_H
