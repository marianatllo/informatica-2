#include "wormsplayerone.h"
#include <QGraphicsScene>
#include <QDebug>
WormsPlayerOne::WormsPlayerOne(double px, double py, QList<Objects *> _listObs, Player _player)
{
    move = new Move(px,py);//Instancio un objeto de tipo Move

    QFont font("Cooper Black",10,QFont::Normal,false);//Instruccion para el tipo de fuente del text
    text = new QGraphicsTextItem(this);//Creo un nuevo item llamado text
    text->setPlainText(QString::number(life));//La clase Plaintext, que se utiliza para editar y mostrar el texto llamado life
    text->setFont(font);//Agrego la fuente al text


    if(_player==Player::ONE){//Si el jugador es el jugador1
         pix.load(":/Worms/Worms/worm1.png");//Se agrega la imagen del gusano
         text->setPos(0,-20);//Se le asigna a el texto  una posicion, con la intencion de que sea encima de cada gusano
    }else{//De lo contrario si es el jugador 2
         pix.load(":/Worms/Worms/worm1R.png");//Agrega la misma imagen pero hacia el otro lado
         text->setPos(-5,-20);
    }

    listObs = _listObs;

    tMove[0] = new QTimer(this);//Timer para mover los gusanos en Y (salto)
    connect(tMove[0],SIGNAL(timeout()),this,SLOT(movePlayerOne()));
    tMove[0]->stop();//Empieza en stop

    tMove[1] = new QTimer(this);//Timer para mover los gusanos hacia izq y der.
    connect(tMove[1],SIGNAL(timeout()),this,SLOT(movePlayerOneX()));
    tMove[1]->stop();//Empieza en stop

    player=_player;

    setPos(px,-py);//ACtualiza la posicion
}

WormsPlayerOne::~WormsPlayerOne()
{
    delete move;
    delete tMove[0];
    delete tMove[1];

}

QRectF WormsPlayerOne::boundingRect() const
{
    return QRectF(0,0,pix.width(),pix.height());
}

void WormsPlayerOne::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->drawPixmap(0,0,pix.width(),pix.height(),pix);
}

void WormsPlayerOne::mousePressEvent(QGraphicsSceneMouseEvent *)
{
    if(stateFocus==true){//Si la bandera es true
        setFlags(QGraphicsItem::ItemIsFocusable);//El item seleccionado se enfoca, para saber con que gusano voy a jugar
        setFocus();
    }

}

void WormsPlayerOne::keyPressEvent(QKeyEvent *t)
{
    if(player == WormsPlayerOne::ONE){//Si el jugaro es el jugador1, se determinan las teclas con que se va a mover el gusano1
        if(ban1==0){
            if(t->key()==Qt::Key_W){//Con la tecla W
                move->setVy(40);//Se le asigna la velocidad en y
                tMove[0]->start(10);//Se activa el timer para mover
                ban1=2;
            }else if(t->key()==Qt::Key_S){//con la tecla S
                tMove[1]->stop();//El timer se pone en stop lo que hace que el gusano se quede quieto y no se mueva a la derecha o izq.
            }
        }
            if(t->key()==Qt::Key_D){
                pix.load(":/Worms/Worms/worm1.png");//Con la tecla D que se mueve hacia la derecha, se coloca la imagen del gusano acorde a su mvto
                tMove[1]->start(10);

                banMoveX=1;
            }
            if(t->key()==Qt::Key_A){
                tMove[1]->start(10);
                banMoveX=2;
                pix.load(":/Worms/Worms/worm1R.png");//Con la tecla A que se mueve hacia la izquierda, se coloca la imagen del gusano acorde a su mvto
            }           
    }else{
        if(ban1==0){
            if(t->key()==Qt::Key_I){
                move->setVy(40);
                tMove[0]->start(10);

                ban1=2;

            }else if(t->key()==Qt::Key_K){
                tMove[1]->stop();
            }

        }
            if(t->key()==Qt::Key_L){
                pix.load(":/Worms/Worms/worm1.png");
                tMove[1]->start(10);
                banMoveX=1;

            }
            if(t->key()==Qt::Key_J){
                tMove[1]->start(10);
                banMoveX=2;
                pix.load(":/Worms/Worms/worm1R.png");
            }
    }
}


void WormsPlayerOne::collision()
{
    for(int i = 0 ; i < listObs.size() ; i++){//Recorro la lista de todos los objetos puesto en escena
        if(move->getY()+listObs.at(i)->y() > 0 && move->getY()+listObs.at(i)->y()<=pix.height()){//Condicional que me resta la posicion 0,0 del gusano con la posicion 0,0 del objeto
            if(collidesWithItem(listObs.at(i))){//Si colisionan
                move->setVy(0);//Se coloca la velocidad del gusano en 0 para que se quede quieto
                move->setY((-listObs.at(i)->y()+pix.height()));//Se actualiza la posicion del gusano a ariba del objeto
                ban1=0;
            }

        }
    }
}

WormsPlayerOne::Player WormsPlayerOne::getPlayer()
{
    return player;
}

int WormsPlayerOne::getLife()
{
    return life;
}

void WormsPlayerOne::setLife(int _life)
{
    if(life>0){//si la vida sigue siendo mayor que cero
        life=_life;
        text->setPlainText(QString::number(life));//se actualiza el text en la escena
    }

}

QGraphicsTextItem *WormsPlayerOne::getTextItem()
{
    return text;
}

void WormsPlayerOne::setIsFocusable(bool isFocusable)
{
    stateFocus= isFocusable;
}

void WormsPlayerOne::movePlayerOne()
{
    move->movement(0.1);//Llamamos a la funcion movement de la clase move, para el mvto en y del gusano
    setPos(move->getX(),-move->getY());//Actualizamos su posicion
    collision();//Llamamos la funcion que colisiona con los objetos de la escena

}

void WormsPlayerOne::movePlayerOneX()
{

    if(banMoveX==1){//Si se presiona las teclas que van a la derecha
        move->setX(x()+0.5);//Aumenta la posicion en X
        setPos(move->getX(),y());//Actualiza la posicion

    }else {
        move->setX(x()-0.5);//Disminuye la posicion es X
        setPos(move->getX(),y());//Actualiza la posicion
    }
}



