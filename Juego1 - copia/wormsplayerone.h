#ifndef WORMSPLAYERONE_H
#define WORMSPLAYERONE_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QTimer>
#include <QMouseEvent>
#include <QKeyEvent>

#include "move.h"
#include "objects.h"
#include "bullets.h"
#include "movebullet.h"

class WormsPlayerOne : public QGraphicsObject
{
    Q_OBJECT

public:
    enum Player{ONE, TWO, NOT};//Declaracion de un tipo de enumeracion para saber que jugador es.
    WormsPlayerOne(double px, double py,QList<Objects*> _listObs, Player _player);//Constructor que me recibe la posicion en x,y, la lista de objetos y el numero de jugador
    ~WormsPlayerOne();
    QRectF boundingRect() const;
    void paint(QPainter *painter,const QStyleOptionGraphicsItem *,QWidget *);
    void mousePressEvent(QGraphicsSceneMouseEvent *);//Click para enfocar el gusano deseado
    void keyPressEvent(QKeyEvent *t);//Funcion para mover los gusanos con las teclas
    void collision();//Funcion para la colision del gusano con los objetos de la escena
    Player getPlayer();//Metodo get de la variable tipo player
    int getLife();
    void setLife(int _life);
    QGraphicsTextItem *getTextItem();//Item creado como texto para la vida
    void setIsFocusable(bool isFocusable);//Funcion para saber si el gusano es enfocado


public slots:
    //Slots para mover el gusano
    void movePlayerOne();//Mvto en Y del gusano (Salto)
    void movePlayerOneX();//Mvto izq y der del gusano


private:
    Move *move;//Instacio la clase Move
    QPixmap pix;//Creo un pixmap
    QTimer *tMove[2];//Arreglo de timer
    QList<Objects*> listObs;//Lista para los objetos
    WormsPlayerOne::Player player;//Variable tipo player
    int ban1=0, banMoveX=0;//Banderas
    QGraphicsTextItem *text;//Creo item de texto
    bool stateFocus = true;

    int life=100;//La vida la inicializo en 100
};



#endif // WORMSPLAYERONE_H
